// simple_xml_settings.h



#ifndef SIMPLE_XML_SETTINGS_SIMPLE_XML_SETTINGS_H
#define SIMPLE_XML_SETTINGS_SIMPLE_XML_SETTINGS_H



#include <tinyxml.h>

#include <iostream>
#include <string>
#include <boost/lexical_cast.hpp>
#include <boost/interprocess/sync/file_lock.hpp>



class SimpleXMLSettings
{
public:
    enum BoolAlpha { TrueFalse , YesNo };

private:
    const std::string pathXml;
    const std::string name;
    bool saving, loading, loadedOk;
    BoolAlpha boolAlpha;

    boost::interprocess::file_lock* fileLock;

    TiXmlDocument doc;
    TiXmlElement* currElement;

public:
    SimpleXMLSettings(const std::string & path, const std::string & elementName = "", BoolAlpha boolAlpha = TrueFalse);

    void SetBoolAlpha(BoolAlpha value);
    bool HasLock();

    void BeginSave();
    void EndSave();

    void BeginLoad();
    void EndLoad();

    bool BeginGroup(const std::string & groupName, int index = 0);
    bool BeginGroup(const std::string & groupName, const std::string & key, const std::string & value, int index = 0);
    void EndGroup();

    template<typename VALUE_TYPE>
    void SaveNumber(const std::string & key, const VALUE_TYPE & value)
    {
        if (!(loading || saving))
            throw "You should call beginSave()";

        // Add key value pair as a new attribute
        std::string strValue = std::to_string(value);
        currElement->SetAttribute(key.c_str(), strValue.c_str());
    }

    void SaveText(const std::string & key, const std::string & value);
    void SaveText(const std::string & key, char value);
    void SaveBool(const std::string & key, const bool & value);

    const char* Load(const std::string & key);

    template<typename VALUE_TYPE>
    VALUE_TYPE LoadNumber(const std::string & key, VALUE_TYPE defaultValue)
    {
        const char* value = Load(key);

        if (!loadedOk)
            return defaultValue;

        try
        {
            return boost::lexical_cast<VALUE_TYPE>( std::string(value) );
        }
        catch (boost::bad_lexical_cast &)
        {
            return defaultValue;
        }
    }

    std::string LoadText(const std::string & key, std::string defaultValue);
    char LoadText(const std::string & key, char defaultValue);
    bool LoadBool(const std::string & key, bool defaultValue);

    bool Loaded();

    ~SimpleXMLSettings();
};



#endif
