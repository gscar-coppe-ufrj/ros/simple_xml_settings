// simple_xml_settings.cpp



#include <simple_xml_settings/simple_xml_settings.h>

#include <iostream>
#include <fstream>
#include <string>
#include <boost/lexical_cast.hpp>
#include <boost/interprocess/sync/file_lock.hpp>



SimpleXMLSettings::SimpleXMLSettings(const std::string & path, const std::string & elementName, BoolAlpha boolalpha) :
    pathXml(path), name(elementName), saving(false), loading(false), loadedOk(false), boolAlpha(boolalpha)
{
    try
    {
        fileLock = new boost::interprocess::file_lock(path.c_str());
    }
    catch (boost::interprocess::interprocess_exception & exception)
    {
        std::cout << "Warning - File \"" << path << "\" does not exist or there is no resource available. Attempting to create it";
        try
        {
            std::ofstream file(path.c_str());
            file.close();

            fileLock = new boost::interprocess::file_lock(path.c_str());
        }
        catch (boost::interprocess::interprocess_exception & exception)
        {
            std::cout << "Warning - No resource available for the file \"" << path << "\"";
            fileLock = nullptr;
            std::cout << exception.what() << std::endl;
        }
    }
}


void SimpleXMLSettings::SetBoolAlpha(SimpleXMLSettings::BoolAlpha value)
{
    boolAlpha = value;
}


bool SimpleXMLSettings::HasLock()
{
    return fileLock != nullptr;
}


void SimpleXMLSettings::BeginSave()
{
    if (loading || saving)
        throw "Already saving or loading";

    // Lock file
    fileLock->lock();

    //doc.LoadFile( pathXml.c_str() );

    TiXmlElement* root = doc.FirstChildElement();
    if (!root)
    {
        std::cout << "Warning - File did not have a root element. Creating one named root." << std::endl;
        root = new TiXmlElement("root");
        doc.LinkEndChild(root);
    }

    if (!name.empty())
    {
        currElement = root->FirstChildElement(name.c_str());
        if (!currElement)
        {
            currElement = new TiXmlElement(name.c_str());
            root->LinkEndChild(currElement);
        }
    }
    else
        currElement = root;

    saving = true;
}


void SimpleXMLSettings::EndSave()
{
    if (!(loading || saving))
        throw "You should call beginSave()";

    // Save modifications to XML file
    doc.SaveFile( pathXml.c_str() );

    // Clear allocated memory
    doc.Clear();

    // Unlock file
    fileLock->unlock();

    saving = false;
}


void SimpleXMLSettings::BeginLoad()
{
    if (loading || saving)
        throw "Already saving or loading";

    // Lock file with a sharable lock, since we are only reading
    fileLock->lock_sharable();

    doc.LoadFile(pathXml.c_str());

    TiXmlElement* root = doc.FirstChildElement();

    if (name.empty())
        currElement = root;
    else
        currElement = (root) ? root->FirstChildElement(name.c_str()) : nullptr;

    loading = true;
}


void SimpleXMLSettings::EndLoad()
{
    if (!(loading || saving))
        throw "You should call beginLoad()";

    // Clear allocated memory
    doc.Clear();

    // Unlock file
    fileLock->unlock_sharable();

    loading = false;
}


bool SimpleXMLSettings::BeginGroup(const std::string & groupName, int index)
{
    if (!(loading || saving))
        throw "You should call beginSave() or beginLoad()";

    // Happens when loading and base element does not exist
    if (!currElement)
        return false;

    TiXmlNode * parent = currElement;
    TiXmlHandle handle(currElement);
    currElement = handle.ChildElement(groupName.c_str(), index).ToElement();
    if (!currElement)
    {
        if (saving)
        {
            currElement = new TiXmlElement(groupName.c_str());
            parent->LinkEndChild(currElement);
            return true;
        }
        else if (loading)
        {
            currElement = parent->ToElement();
            return false;
        }
    }

    return true;
}


bool SimpleXMLSettings::BeginGroup(const std::string & groupName, const std::string & key, const std::string & value, int index)
{
    if (!loading)
        throw "This function is only supposed to be called when loading.";

    int i = 0, found = 0;
    while ( BeginGroup(groupName, i++) )
    {
        const char * value_loaded = Load(key);
        if (loadedOk)
        {
            if (value == value_loaded)
            {
                if (found++ == index)
                    return true;
            }
        }
        EndGroup();
    }

    return false;
}


void SimpleXMLSettings::EndGroup()
{
    if (!(loading || saving))
        throw "You should call beginSave() or beginLoad()";

    currElement = currElement->Parent()->ToElement();
}


void SimpleXMLSettings::SaveText(const std::string &key, const std::string &value)
{
    if (!(loading || saving))
        throw "You should call beginSave()";

    // Add key value pair as a new attribute
    currElement->SetAttribute(key.c_str(), value.c_str());
}


void SimpleXMLSettings::SaveText(const std::string &key, char value)
{
    this->SaveText(key, std::string(1, value));
}


void SimpleXMLSettings::SaveBool(const std::string &key, const bool &value)
{
    if (!(loading || saving))
        throw "You should call beginSave()";

    std::string strValue;
    switch (boolAlpha)
    {
    case YesNo:
        strValue = (value) ? "yes" : "no";
        break;
    case TrueFalse:
    default:
        strValue = (value) ? "true" : "false";
        break;
    }

    // Add key value pair as a new attribute
    currElement->SetAttribute(key.c_str(), strValue.c_str());
}


const char* SimpleXMLSettings::Load(const std::string & key)
{
    if (!(loading || saving))
        throw "You should call beginLoad()";

    loadedOk = true;

    if (currElement == nullptr)
    {
        loadedOk = false;
        return "";
    }

    const char* value = currElement->Attribute( key.c_str() );

    if (value == nullptr)
        loadedOk = false;

    return value;
}


std::string SimpleXMLSettings::LoadText(const std::string & key, std::string defaultValue)
{
    const char* value = Load(key);

    if (!loadedOk)
        return defaultValue;

    return value;
}


char SimpleXMLSettings::LoadText(const std::string & key, char defaultValue)
{
    std::string text = this->LoadText(key, std::string(1, defaultValue));
    return text.at(0);
}


bool SimpleXMLSettings::LoadBool(const std::string & key, bool defaultValue)
{
    const char* value = Load(key);

    if (!loadedOk)
        return defaultValue;

    switch (boolAlpha)
    {
    case YesNo:
        return strcmp(value, "yes") == 0;
    case TrueFalse:
    default:
        return strcmp(value, "true") == 0;
    }
}


bool SimpleXMLSettings::Loaded()
{
    return loadedOk;
}


SimpleXMLSettings::~SimpleXMLSettings()
{
    if (fileLock)
        delete fileLock;
}
